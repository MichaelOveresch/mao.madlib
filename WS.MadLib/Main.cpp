	
// Mad Lib

#include <string>
#include <fstream>
#include <iostream>
#include <conio.h>

using namespace std;

char save;
string Questions[15] = { "an adjective", "a nationality", "a person", "a noun", "an adjective", "a noun", "an adjective", "an adjective", "a plural noun", "a noun", "a number", "a plural shape", "a food", "a food", "a number"};

void PrintWords(string* Words, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "\n Enter " << Questions[i] << ": ";
		getline(cin, Words[i]);
	}
}

int main()
{
	const int SIZE = 15;
	string words[15];

	PrintWords(words, SIZE);

	string story = "\nPizza was invented by a " + words[0] + " " + words[1] + " chef named " + words[2] + "."
		+ "\nTo make a pizza, you need to take a lump of " + words[3] + ", and make a thin, round " + words[4] + " " + words[5] + "."
		+ "\nThen you cover it with " + words[6] + " sauce, " + words[7] + " cheese, and fresh chopped " + words[8] + "."
		+ "\nNext you have to bake it in a very hot " + words[9] + "."
		+ "\nWhen it is done, cut it into " + words[10] + " " + words[11] + "."
		+ "\nSome kids like " + words[12] + " pizza the best, but my favorite is the " + words[13] + " pizza."
		+ "\nIf I could, I would eat pizza " + words[14] + " times a day!";

	cout << endl << story;

	cout << endl << "Do you want to save? (y/n) ";
	cin >> save;
	if (save == 'Y' || save == 'y')
	{
		string filepath = "C:\\Users\\william272\\Desktop\\Pizza Mad Lib.txt";
		ofstream ofs(filepath);
		ofs << story;
		ofs.close();
		cout << endl << "Your Pizza Mad Lib has been saved.";
		cout << endl << "Press any key to exit";
	}
	else
	{
		cout << endl << "Press any key to exit";
	}

	(void)_getch();
	return 0;
}
